#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,x1,x2,disc;
	printf("enter coefficients of the quadratic equations\n");
	scanf("%f%f%f",&a,&b,&c);
	disc=(b*b)-(4*a*c);
	if (disc==0)
	{
		x1=x2=-b/(2*a);
		printf("the roots are real and equal\n");
		printf("x1=%f \n x2=%f",x1,x2);
	}
	else if(disc>0)
	{
		x1=-b+(sqrt(disc)/(2*a));
		x2=-b-(sqrt(disc)/(2*a));
		printf("the roots are distinct\n");
		printf("x1=%f \n x2=%f",x1,x2);
	}
	else
	{
		printf("the roots are immaginary\n");
		printf("x1=%f+i%f \n x2=%f-i%f",-b/(2*a),disc/(2*a),-b/(2*a),disc/(2*a));
	}
	return 0;
}